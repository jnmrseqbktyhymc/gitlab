import { s__ } from '~/locale';

export const STORAGE_STATISTICS_USAGE_QUOTA_LEARN_MORE = s__(
  'UsageQuota|Learn more about usage quotas.',
);

export const NAMESPACE_STORAGE_OVERVIEW_SUBTITLE = s__('UsageQuota|Namespace overview');
export const NAMESPACE_STORAGE_BREAKDOWN_SUBTITLE = s__('UsageQuota|Storage usage breakdown');
export const NAMESPACE_STORAGE_ERROR_MESSAGE = s__(
  'UsageQuota|Something went wrong while loading usage details',
);

export const STORAGE_STATISTICS_NAMESPACE_STORAGE_USED = s__('UsageQuota|Namespace storage used');

export const STORAGE_STATISTICS_PURCHASED_STORAGE_USED = s__('UsageQuota|Purchased storage used');

export const STORAGE_STATISTICS_PURCHASED_STORAGE = s__('UsageQuota|Purchased storage');

export const BUY_STORAGE = s__('UsageQuota|Buy storage');
